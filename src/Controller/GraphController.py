from Model.TwitterListener import TwitterListener
from tweepy import Stream
from tweepy import OAuthHandler
from View.Widgets.TweetProgressBar import TweetProgressBar
from View.GraphView import GraphView
from Model.TwitterAnalyzer import TwitterAnalyzer
from PySide.QtCore import *


class GraphController(object):

    def __init__(self, mainWindow):
        self.mainWindow = mainWindow
        self.twitterAnalyzer = TwitterAnalyzer()
        self.twitterListener = TwitterListener(self.twitterAnalyzer)
        self.graphView = GraphView(mainWindow, self, self.twitterAnalyzer, self.twitterListener)




    def streamTweets(self, fileName):
        self.twitterListener.setSaveFile(fileName)

        consumerKey = "FDjfrSreOLOjB1B8WOdhwm24a"
        consumerSecret = "nS1QzOFoVItT3X5mja4BBTCPV30Kc5Yjg4u56bwMWsgbyICAyZ"
        accessToken = "2463534414-NvT8vv2QxdENXjSpgkbtMH6OPzz3fZPlR4v0mnp"
        tokenSecret = "TGOVccoet9PB3TLsoAFLBAah4JaLVt0EI3sbKx9Xryf4C"

        auth = OAuthHandler(consumerKey, consumerSecret)
        auth.set_access_token(accessToken, tokenSecret)
        twitterStream = Stream(auth, self.twitterListener)
        twitterStream.filter(track=['weather'], languages=['en'], async=True)

        #call a progress popup before the actual loading start
        self.tpb = TweetProgressBar(self.twitterListener)
        self.twitterAnalyzer.resetData()
        self.twitterListener.startStream(twitterStream)





    def lineGraphAnalyzer(self):
        lineGraphData = []
        #if line graph data is empty, analyze data
        if not self.twitterAnalyzer.getLineGraphData():
            GraphController.analyzeData(self)
            lineGraphData = self.twitterAnalyzer.getLineGraphData()
        else:
            lineGraphData = self.twitterAnalyzer.getLineGraphData()
        #print(lineGraphData[0][0], lineGraphData[0][1], lineGraphData[0][2])
        return lineGraphData




    def pieGraphAnalyzer(self):
        if not self.twitterAnalyzer.getLineGraphData():
            GraphController.analyzeData(self)
        pieGraphData = []
        pieGraphData.append(self.twitterAnalyzer.getGoodWeather())
        pieGraphData.append(self.twitterAnalyzer.getBadWeather())
        return pieGraphData




    def getView(self):
        return self.graphView



