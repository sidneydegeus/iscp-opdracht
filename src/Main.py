import sys

from PySide import QtGui

from MainWindow import MainWindow


def main():
    app = QtGui.QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    sys.exit(app.exec_())




if __name__ == "__main__":
    main()