from PySide import QtGui
from Controller.GraphController import GraphController


class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent = None):
        super(MainWindow, self).__init__(parent)

        #Standard Main Window data
        self.setWindowTitle("Twitter Analyse")
        self.setGeometry(300, 200, 1024, 768)
        self.setFixedSize(self.size())
        self.setBackgroundRole(QtGui.QPalette.Midlight)
        self.setAutoFillBackground(True)

        self.goToAnalayzeTweets()



    def display(self, view):
        theView = view(self).getView()
        theView.initUI()
        self.form_widget = theView
        self.setCentralWidget(self.form_widget)
        self.show()




    def goToAnalayzeTweets(self):
        MainWindow.display(self, GraphController)










