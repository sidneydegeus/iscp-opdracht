from PySide.QtGui import *
from PySide.QtCore import *
import datetime
import sys
import os
import matplotlib.dates as mdates
import View.Widgets.Graph as graph
import matplotlib.animation as animation

class GraphView(QWidget):

    def __init__(self, parent, graphController, twitterAnalyzer, twitterListener):
        super(GraphView, self).__init__(parent)
        self.graphController = graphController
        self.twitterAnalyzer = twitterAnalyzer
        self.twitterListener = twitterListener
        self.activeGraph = "none"




    def initUI(self):
        self.graph = graph.Graph(self.twitterAnalyzer, self.twitterListener)

        #Tweet buttons
        self.saveTweets = QPushButton('Save tweets to file', self)
        self.saveTweets.setToolTip("Save tweets to a file and display data realtime")
        self.saveTweets.clicked.connect(self.saveFileDialog)

        self.loadTweets = QPushButton('Load tweets from a file', self)
        self.loadTweets.setToolTip("Load the tweets that were saved to a file in order to analyze them")
        self.loadTweets.clicked.connect(self.openFileDialog)


        #label title
        self.title = QLabel("Current file loaded:\n None")
        self.title.setAlignment(Qt.AlignCenter)
        self.title.setFont(QFont('SansSerif', 24))


        #graph buttons
        self.plotButton = QPushButton('Plot')
        self.plotButton.clicked.connect(self.graph.plot)
        self.pieButton = QPushButton('Pie')
        self.pieButton.clicked.connect(self.graph.pie)


        #build layout
        layout = QVBoxLayout()

        tweetButtonLayout = QHBoxLayout()
        tweetButtonLayout.addWidget(self.saveTweets)
        tweetButtonLayout.addWidget(self.loadTweets)
        layout.addLayout(tweetButtonLayout)


        layout.addWidget(self.title)
        layout.addWidget(self.graph)

        graphButtonLayout = QHBoxLayout()
        graphButtonLayout.addWidget(self.plotButton)
        graphButtonLayout.addWidget(self.pieButton)
        layout.addLayout(graphButtonLayout)

        self.setLayout(layout)




    def saveFileDialog(self):
        i = datetime.datetime.now()
        extension = ".csv"
        standardFileName = "TweetData {}-{}-{}{}".format(i.day, i.month, i.year, extension)

        fileName, _ = QFileDialog.getSaveFileName(
            self,
            "Save Tweets", #title of dialog
            os.path.expanduser("~/{}".format(standardFileName)), #user directory + standard file name
            extension #extensions
        )
        print(fileName)
        try:
            #temporary solution....
            if (fileName is not ''):
                self.graphController.streamTweets(fileName)
        except Exception as e:
            print ("Canceled save dialog")
            print ("Unexpected error:", sys.exc_info()[0])
            print(repr(e))




    def openFileDialog(self):
        fileName, _ = QFileDialog.getOpenFileName(
            self,
            "Open Tweet File",
            "",
            "Tweet Files (*.csv)"
        )
        print(fileName)
        try:
            #temporary solution....
            if (fileName is not ''):
                self.title.setText("Current file loaded:\n" + fileName)
                self.twitterAnalyzer.loadTweets(fileName)
                if (self.activeGraph == "plot"):
                    self.graph.plot()
                elif(self.activeGraph == "pie"):
                    self.graph.pie()
        except Exception as e:
            print ("Canceled save dialog")
            print ("Unexpected error:", sys.exc_info()[0])
            print(repr(e))




