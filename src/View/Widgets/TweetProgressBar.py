from PySide.QtGui import *
from PySide.QtCore import Qt
import atexit

class TweetProgressBar(QWidget):

    #this is a widget that represents the "progress bar" of the tweets collected.

    def __init__(self, twitterListener, parent = None):
        super(TweetProgressBar, self).__init__(parent)
        self.twitterListener = twitterListener
        self.twitterListener.registerObserver(self)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.initUI()





    def initUI(self):
        #Widget
        self.setWindowTitle("Tweet Progress")
        self.setGeometry(500, 400, 250, 100)
        self.setFixedSize(self.size())

        flags = Qt.WindowFlags(
            Qt.WindowMaximizeButtonHint,
            Qt.WindowStaysOnTopHint,
            Qt.X11BypassWindowManagerHint
        )
        self.setWindowFlags(flags)

        #widget layout
        self.layout = QVBoxLayout(self)

        #widget additions
        self.progressTextLabel = QLabel()
        self.progressTextLabel.setText("Tweets collected: {} / 10000".format(self.twitterListener.getTweetCount()))

        self.progressPercentLabel = QLabel()
        self.progressPercentLabel.setText("0% Complete")

        self.layout.addWidget(self.progressTextLabel)
        self.layout.addWidget(self.progressPercentLabel)

        self.show()





    def update(self):
        self.progressTextLabel.setText("Tweets collected: {} / 10000".format(self.twitterListener.getTweetCount()))
        #self.repaint()
        self.progressPercentLabel.setText("{}% Complete".format(self.twitterListener.getTweetCount() / 10000 * 100))





    def closeEvent(self, event):
        print("stop loader")
        self.twitterListener.removeObserver(self)
        self.destroy()

