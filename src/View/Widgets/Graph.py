from PySide.QtGui import *
import matplotlib.dates as mdates
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
import matplotlib.pyplot as plt

class Graph(QWidget):

    #this is a widget that represents the "progress bar" of the tweets collected.

    def __init__(self, twitterAnalyzer, twitterListener, parent=None):
        super(Graph, self).__init__(parent)
        self.twitterListener = twitterListener
        self.twitterAnalyzer = twitterAnalyzer
        self.twitterAnalyzer.registerObserver(self)
        self.activeGraph = ""
        self.initUI()




    def initUI(self):
        #matplotlib display
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        toolbar = NavigationToolbar(self.canvas, self)

        layout = QVBoxLayout(self)
        layout.addWidget(toolbar)
        layout.addWidget(self.canvas)




    def plot(self):
        self.activeGraph = "plot"
        self.figure.clf()
        lineGraphData = self.twitterAnalyzer.getLineGraphData()
        x = []
        dataGoodWeather = []
        dataBadWeather = []

        for i in range(len(lineGraphData)):
            x.append(lineGraphData[i][0])
            dataGoodWeather.append(lineGraphData[i][1])
            dataBadWeather.append(lineGraphData[i][2])

        ax = self.figure.add_subplot(111)
        ax.plot_date(x, dataGoodWeather, label='Good Weather', color='g', linewidth=1, linestyle='solid', marker='None')
        ax.plot_date(x, dataBadWeather, label='Bad weather', color='r', linewidth=1, linestyle='solid', marker='None')
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
        ax.set_ylabel('Total')
        ax.set_xlabel('Time')
        ax.set_title('Line graph')
        ax.legend()
        self.canvas.draw()




    def pie(self):
        self.activeGraph = "pie"
        self.figure.clf()
        slices = [self.twitterAnalyzer.getGoodWeather(), self.twitterAnalyzer.getBadWeather()]
        weatherTypes = ['Good weather', 'Bad weather']
        cols = ['g', 'r']
        ax = self.figure.add_subplot(111)
        ax.pie(slices, labels=weatherTypes, colors=cols, startangle=90, autopct='%1.1f%%')
        ax.set_title('Pie graph')
        self.canvas.draw()




    def update(self):
        #self.twitterAnalyzer.loadTweets(self.twitterListener.getSaveFile())
        if self.activeGraph == "plot":
            self.plot()
        elif self.activeGraph == "pie":
            self.pie()

