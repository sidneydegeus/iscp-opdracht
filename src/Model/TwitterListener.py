from tweepy.streaming import StreamListener
import time

class TwitterListener(StreamListener):

    def __init__(self, twitterAnalyzer, parent = None):
        StreamListener.__init__(self, parent)
        self.exiting = False
        self.tweetCount = 0
        self.saveFile = ""
        self.observerList = []
        self.twitterAnalyzer = twitterAnalyzer




    def __exit__(self):
        self.exiting = True



    #SteamListener implementations
    def on_data(self, data):
        saveFile = open(self.saveFile, "a")
        try:
            if self.tweetCount <= 10000 and self.exiting == False:
                #print (self.tweetCount)
                #print (data)
                saveFile.write(data)
                self.appendTweetData(data)
                self.notifyObservers()
                self.tweetCount += 1
                return True
            else:
                return False
        except BaseException as e:
            #In case connection drops or anything, throw exception
            print ("Something went wrong with ondata. ", str(e))
            time.sleep(10)
        finally:
            saveFile.close()




    def on_error(self, status):
        print (status)
        return False




    def startStream(self, twitterStream):
        self.twitterStream = twitterStream




    #observer
    def registerObserver(self, observer):
        self.observerList.append(observer)

    def removeObserver(self, observer):
        self.observerList.remove(observer)
        self.__exit__()

    def notifyObservers(self):
        for observer in self.observerList:
            observer.update()




    #getters & setters
    def getTweetCount(self):
        return self.tweetCount

    def setSaveFile(self, file):
        self.saveFile = file

    def getSaveFile(self):
        return self.saveFile

    def appendTweetData(self, data):
        self.twitterAnalyzer.appendRawData(data)

