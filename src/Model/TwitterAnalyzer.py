import sys
from datetime import time, datetime

class TwitterAnalyzer():

    def __init__(self):
        self.observerList = []
        self.weatherReferences()
        #data lists
        self.rawData = []
        self.lineGraphData = []
        #counters
        self.goodWeather = 0
        self.badWeather = 0
        self.otherWeather = 0



    def resetData(self):
        self.lineGraphData = []
        self.goodWeather = 0
        self.badWeather = 0
        self.otherWeather = 0



    def loadTweets(self, file):
        with open(file, "r") as ins:
            rawData = []
            for line in ins:
                if line.strip():
                    rawData.append(line)
            ins.close()
            self.setRawData(rawData)
            #reset counters in case there is any
            self.resetData()
        self.analyzeData()



    def analyzeData(self):
        lineGraphData = []
        goodWeatherReferences = self.getGoodWeatherReferences()
        badWeatherReferences = self.getBadWeatherReferences()
        rawData = self.getRawData()
        for tweet in rawData:
            tweetData = tweet.split(',')
            try:
                tweetDatetime = tweetData[0].split(' ')
                tweetTime = datetime.strptime(tweetDatetime[3], '%H:%M:%S')
                lineGraphData.append([
                    tweetTime,
                    self.getGoodWeather(),
                    self.getBadWeather(),
                    self.getOtherWeather()
                ])
            except Exception as e:
                pass

            tweetText = ""
            try:
                tweetText = tweetData[3]
            except Exception as e:
                pass

            tweetText.lower()
            if any(weather in tweetText for weather in goodWeatherReferences):
                self.addOneGoodWeather()
            elif any(weather in tweetText for weather in badWeatherReferences):
                self.addOneBadWeather()
            else:
                self.addOneOtherWeather()
            self.setLineGraphData(lineGraphData)



    def analyzeStreamedTweet(self):
        lineGraphData = []
        goodWeatherReferences = self.getGoodWeatherReferences()
        badWeatherReferences = self.getBadWeatherReferences()
        latestTweet = self.rawData[len(self.rawData) - 1]
        tweetData = latestTweet.split(',')

        tweetDatetime = tweetData[0].split(' ')
        tweetTime = datetime.strptime(tweetDatetime[3], '%H:%M:%S')
        lineGraphData.append(tweetTime)
        lineGraphData.append(self.getGoodWeather())
        lineGraphData.append(self.getBadWeather())
        lineGraphData.append(self.getOtherWeather())

        tweetText = ""
        try:
            tweetText = tweetData[3]
        except Exception as e:
            pass

        tweetText.lower()
        if any(weather in tweetText for weather in goodWeatherReferences):
            self.addOneGoodWeather()
        elif any(weather in tweetText for weather in badWeatherReferences):
            self.addOneBadWeather()
        else:
            self.addOneOtherWeather()
        self.appendLineGraphData(lineGraphData)



    def weatherReferences(self):
        self.goodWeatherReferences = [
            #good
            'good weather',
            'weather is good',
            'weather so good',
            'weather is so good',
            'weather looking good',
            'weather is looking good',
            #nice
            'nice weather',
            'weather is nice',
            #beautiful
            'beautiful weather',
            'weather is beautiful',
            #lovely
            'lovely weather',
            'weather is lovely',
            #hot
            'hot weather',
            'weather is hot',
            #warm
            'warm weather',
            'weather is warm',
            'warm regular weather',
            #sunny
            'sunny weather',
            'weather is sunny',
            #great
            'great weather',
            'weather is great',
            #cool
            'cool weather',
            'weather is cool',
            #other
            'enjoying the weather',
            'perfect weather',
            'weather is perfect',
            'summer weather',
            #references with no weather to it
            'mostly sunny',
            'pretty nice'
        ]
        self.badWeatherReferences = [
            #severe
            'severe weather',
            'weather is severe',
            #bad
            'bad weather',
            'weather is bad',
            'weather so bad',
            'weather is so bad',
            #terrible
            'terrible weather',
            'weather is terrible',
            #cold
            'cold weather',
            'weather is cold',
            #wet
            'wet weather',
            'weather is wet',
            #rainy
            'rainy weather',
            'weather is rainy',
            'it rains all day',
            'raining for days',
            #cloudy
            'cloudy weather',
            'weather is cloudy',
            #shit
            'shit weather',
            'weather is shit',
            #crappy
            'crappy weather',
            'weather is crappy',
            #humid
            'humid weather',
            'weather is humid',
            #stormy
            'stormy weather',
            'weather is stormy',
            #horrible
            'horrible weather',
            'weather is horrible',
            #hazardous
            'hazardous weather',
            'weather is hazardous',
            #depresses
            'weather depresses me',
            'weather is depressive',
            #other
            'hurricane weather',
            'thunder and rain weather',
            'weather sucks',
            'weather is killing me',
            'weather kills me',
            'fucking weather',
            'fucked up weather',
            'winter weather',
            #references with no weather to it
            'mostly cloudy',
            'storm warning',
            'severe storm'
        ]
        self.goodWeather = 0
        self.badWeather = 0
        self.otherWeather = 0



    #getters and setters
    def getGoodWeather(self):
        return self.goodWeather

    def setGoodWeather(self, amount):
        self.goodWeather = amount

    def addOneGoodWeather(self):
        self.goodWeather += 1

    def getBadWeather(self):
        return self.badWeather

    def setBadWeather(self, amount):
        self.badWeather = amount

    def addOneBadWeather(self):
        self.badWeather += 1

    def getOtherWeather(self):
        return self.otherWeather

    def setOtherWeather(self, amount):
        self.otherWeather = amount

    def addOneOtherWeather(self):
        self.otherWeather += 1

    def getGoodWeatherReferences(self):
        return self.goodWeatherReferences

    def getBadWeatherReferences(self):
        return self.badWeatherReferences

    def getRawData(self):
        return self.rawData

    def setRawData(self, data):
        self.rawData = data

    def getLineGraphData(self):
        return self.lineGraphData

    def setLineGraphData(self, lineGraphData):
        self.lineGraphData =lineGraphData

    def appendLineGraphData(self, lineGraphData):
        self.lineGraphData.append(lineGraphData)
        self.notifyObservers()

    def appendRawData(self, rawData):
        if rawData:
            self.rawData.append(rawData)
            self.analyzeStreamedTweet()


    #observer methods
    def registerObserver(self, observer):
        self.observerList.append(observer)

    def removeObserver(self, observer):
        self.observerList.remove(observer)

    def notifyObservers(self):
        for observer in self.observerList:
            observer.update()


